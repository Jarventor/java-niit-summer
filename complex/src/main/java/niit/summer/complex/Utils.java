package niit.summer.complex;

public class Utils {
    public static boolean isEqual(double a, double b) {
        return Math.abs(a - b) < 0.0001;
    }
}
