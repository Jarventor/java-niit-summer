package niit.summer.complex;
import static java.lang.String.*;

public class AlgebraicRepresentation
{
    public double a;
    public double b;
    public AlgebraicRepresentation(double _a,double _b)
    {
        this.a = _a;
        this.b = _b;
    }
    double getReal()
    {
        return this.a;
    }
    double getImaginary()
    {
        return this.b;
    }
}
