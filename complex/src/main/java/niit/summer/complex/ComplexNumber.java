package niit.summer.complex;

import static java.lang.StrictMath.*;
import niit.summer.complex.AlgebraicRepresentation.*;
import niit.summer.complex.TrigonometricRepresentation.*;
import niit.summer.complex.MathOperations.*;
public class ComplexNumber {
    double a;
    double b;
    static ComplexNumber ZERO = new ComplexNumber();

    public ComplexNumber(double _a, double _b)
    {
        this.a = _a;
        this.b = _b;
    }
    public ComplexNumber()
    {
        this.a = 0;
        this.b = 0;
    }
    public ComplexNumber add(ComplexNumber _num)
    {
        return new ComplexNumber(this.a + _num.a, this.b + _num.b);
    }
    public ComplexNumber multiply(ComplexNumber _num)
    {
        return new ComplexNumber(this.a * _num.a - this.b * _num.b, this.a * _num.b + this.b * _num.a);
    }
    public ComplexNumber ZERO()
    {
        return new ComplexNumber();
    }

    public double abs()
    {
        return (double) hypot(this.a, this.b);
    }

    double arg()
    {
        MathOperations mo = new MathOperations();
        if(this.a > 0)
            return Math.atan(this.b / this.a);
        else if (this.b > 0)
            return Math.PI + Math.atan(this.a / this.b);
        else if (this.b < 0)
            return -Math.PI + Math.atan(this.a / this.b);

        return 0;
    }

    AlgebraicRepresentation asAlgebraic()
    {
        return new AlgebraicRepresentation(this.a, this.b);
    }

    TrigonometricRepresentation asTrigonometric()
    {
        return new TrigonometricRepresentation(this.a, this.b);
    }
};


