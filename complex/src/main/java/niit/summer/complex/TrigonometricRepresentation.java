package niit.summer.complex;

import static java.lang.StrictMath.*;
import niit.summer.complex.MathOperations.*;

public class TrigonometricRepresentation
{
    double a;
    double b;
    public TrigonometricRepresentation(double _a,double _b)
    {
        this.a = _a;
        this.b = _b;
    }
    public String getAbsoluteValue()
    {
        String str = new String();
        str =   String.valueOf(hypot(this.a, this.b)) + "*(cos" + String.valueOf(new ComplexNumber(this.a, this.b)) + "+isin" + String.valueOf(new ComplexNumber(this.a, this.b)) + ")";
        return str;
    }

    public double getArgument()
    {
        MathOperations mo = new MathOperations();
        if(this.a > 0)
            return Math.atan(this.b / this.a);
        else if (this.b > 0)
            return Math.PI + Math.atan(this.a / this.b);
        else if (this.b < 0)
            return -Math.PI + Math.atan(this.a / this.b);

        return 0;
    }
}
